//
//  Activity.swift
//  table-view
//
//  Created by Oktavia Citra on 20/08/20.
//  Copyright © 2020 Oktavia Citra. All rights reserved.
//

import Foundation

struct Activity {
    var name: String = "Activity"
    var time: String = "Anytime"
    var image: String = "blank"
}
