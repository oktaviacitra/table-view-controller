//
//  ViewController.swift
//  table-view
//
//  Created by Oktavia Citra on 20/08/20.
//  Copyright © 2020 Oktavia Citra. All rights reserved.
//

import UIKit

class ActivityViewController: UIViewController {
    var activity = Activity()
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = UIImage(named: activity.image)
        nameLabel.text = activity.name
        timeLabel.text = activity.time
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
