//
//  ActivityManager.swift
//  table-view
//
//  Created by Oktavia Citra on 20/08/20.
//  Copyright © 2020 Oktavia Citra. All rights reserved.
//

import Foundation

class ActivityManager {
    private var activities: [Activity]
    
    init() {
        activities = [
            Activity(name: "Ride a Bike", time: "Sun, 07 Oct 2020 08:00", image: "bike"),
            Activity(name: "Dinner with Him ❤️", time: "Sun, 07 Oct 2020 19:00", image: "dinner"),
            Activity(name: "Go to National Park", time: "Mon, 08 Oct 2020 09:00", image: "park"),
            Activity(name: "Initial Meeting", time: "Tue, 09 Oct 2020 08:30", image: "meeting")
        ]
    }
    
    func getAll() -> [Activity] {
        return activities
    }
}
